package experiments;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Matrix
{
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        Path path = Paths.get(System.getProperty("user.dir"),"matrix.bin");

        FileOutputStream fileOutputStream = new FileOutputStream(path.toString());
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);

        String heightString = bufferedReader.readLine();
        String widthString = bufferedReader.readLine();

        int height = Integer.parseInt(heightString);
        int width = Integer.parseInt(widthString);

        for( int row = 0; row < height ; row++ )
        {
            String[] rowStringItems;
            while (true)
            {
                String rowString = bufferedReader.readLine();
                rowStringItems = rowString.split(" ");
                if (rowStringItems.length == width)
                {
                    break;
                }
                else
                {
                    System.out.println("Neplatný řádek");
                }
            }

            for (int column = 0; column < width; column++ )
            {
                int number = Integer.parseInt(rowStringItems[column]);
                dataOutputStream.writeInt(number);
            }
        }

        dataOutputStream.close();
    }
}
